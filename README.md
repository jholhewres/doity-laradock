# Doity Laradock

Pacote de módulos para desenvolvedores

## Instalação
```bash
clone
cp env.example .env
sudo docker-compose up -d nginx mysql phpmyadmin workspace # Você pode instalar qualquer módulo
acesse: http://doity.local
```
